import React from "react";
import { ISquare } from "../model";

type SquareProps = {
  value: ISquare;
  onClick: () => void;
};

const Square = ({ value, onClick }: SquareProps) => {
  return (
    <button className="square" onClick={onClick}>
      {value}
    </button>
  );
};

export default Square;
