import React from "react";
import { History } from "../model";

type MovesProps = {
  histories: History[];
  jumpTo: (step: number) => void;
};

const Moves = ({ histories, jumpTo }: MovesProps) => {
  return (
    <ol>
      {histories.map((_, turn) => {
        const desc = turn ? "Go to move #" + turn : "Go to game start";
        return (
          <li key={turn}>
            <button onClick={() => jumpTo(turn)}>{desc}</button>
          </li>
        );
      })}
    </ol>
  );
};

export default Moves;
