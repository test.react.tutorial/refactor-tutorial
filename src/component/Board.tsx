import { ISquare } from "../model";

import Square from "./Square";

type BoardProps = {
  squares: ISquare[];
  onClick: (i: number) => void;
};

const Board = ({ squares, onClick }: BoardProps) => {
  return (
    <div>
      {[...Array(3)].map((_, r) => {
        return (
          <div className="board-row">
            {[...Array(3)].map((_, c) => {
              const cellNumber = 3 * r + c;
              return (
                <Square
                  value={squares[cellNumber]}
                  onClick={() => onClick(cellNumber)}
                />
              );
            })}
          </div>
        );
      })}
    </div>
  );
};

export default Board;
