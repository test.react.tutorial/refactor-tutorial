import React, { useCallback, useState } from "react";

import { History } from "../model";
import { calculateWinner } from "../method";

import Board from "./Board";
import Moves from "./Moves";

const Game = () => {
  const [histories, setHistories] = useState<History[]>([
    {
      squares: Array(9).fill(null),
    },
  ]);

  const [stepNumber, setStepNumber] = useState<number>(0);
  const [xIsNext, setXIsNext] = useState<boolean>(true);

  const handleClick = useCallback(
    (checked: number) => {
      const history = histories.slice(0, stepNumber + 1);
      const current = history[history.length - 1];
      const squares = current.squares.slice();
      if (calculateWinner(squares) || squares[checked]) {
        return;
      }
      squares[checked] = xIsNext ? "X" : "O";

      setHistories(
        histories.concat([
          {
            squares: squares,
          },
        ])
      );
      setStepNumber(histories.length);
      setXIsNext(!xIsNext);
    },
    [histories, stepNumber, xIsNext]
  );

  const jumpTo = (step: number) => {
    setStepNumber(step);
    setXIsNext(step % 2 === 0);
  };

  const currentBoard = histories[stepNumber].squares;
  const winnersCells = calculateWinner(currentBoard);
  const winner = winnersCells ? currentBoard[winnersCells[0]] : null;

  const status = {
    statusText: winner
      ? "Winner: " + winner
      : "Next player: " + (xIsNext ? "X" : "O"),
  };

  return (
    <div className="game">
      <div className="game-board">
        <Board
          squares={currentBoard}
          onClick={(checked: number) => handleClick(checked)}
        />
      </div>
      <div className="game-info">
        <div>{status.statusText}</div>
        <Moves histories={histories} jumpTo={jumpTo} />
      </div>
    </div>
  );
};

export default Game;
